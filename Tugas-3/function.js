console.log(" Soal no 1");

function teriak(){
    return "Halo Sanbers!";
}
console.log(teriak());

console.log(" Soal no 2");

function kalikan(num1, num2){
    return num1 * num2;
}

console.log(kalikan(4, 12));

console.log(" Soal no 3");

function introduce(name, age, address, hobby){
     return (
        "Nama saya " + 
        name +
        ", umur saya " + 
        age + 
        " tahun, alamat saya di " + 
        address + 
        ", dan saya punya hobby yaitu " + 
        hobby + "!" 
     );
}

console.log(introduce("Dean", 17, "Bekasi", "Nonton"));