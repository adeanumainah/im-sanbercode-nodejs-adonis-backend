console.log(" Soal no 1");

console.log("Looping 1");

var i = 2;
while (i <= 20){
    var output = i + " - I love coding";
    console.log(output);
    i += 2;
}

console.log("Looping 2");

var a = 20;
while(a >= 1){
    var output2 = a + " - I will become a mobile developer";
    console.log(output2);
     a -= 2;
}

console.log(" Soal no 2");

for(var u=1; u <= 20; u++){
     var output3 = "";
     if( u % 2 == 1){
        if (u % 3 == 0){
            output3 = u + " - I Love Coding";
        } else {
            output3 = u + " - Santai";
        }
     } else {
        output3 = u + " - Berkualitas";
     }
     console.log(output3);
}

console.log(" Soal no 3");

function makeRectangle(panjang, lebar) {
    for (var p = 1; p <= lebar; p++){
        var line = "";
        for(var l = 1; l <= panjang; l++){
            line += "#";
        }
        console.log(line);
    }
  }
    
  makeRectangle(8,4);

//   console.log(" Soal no 4");

//   function makeLadder(sisi) {
//     for(var r = 1; r <= sisi; r ++){
//         line1 = "";
//         for(var t = r; t <= sisi; t++){
//             line1 += "#"
//         }
//         console.log(line1);
//     }
//   }
  
//   makeLadder(7);

  console.log(" Soal no 4");

  function makeLadder(sisi) {
    var line1 = "";
    for(var r = 1; r <= sisi; r ++){
        for(var t = 7; t <= sisi; t++){
            line1 += "#"
        }
        console.log(line1);
    }
  }
  
  // TEST CASE
  makeLadder(7);

