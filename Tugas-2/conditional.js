console.log("Soal Conditional");

var nama = "dean";
var role = "werewolf";
var welcome = "Selamat datang di Dunia Werewolf, "

if(nama == "" && role == ""){
    console.log("Nama harus diisi!");
} else if(nama != "" && role == ""){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
} else if(nama != ""){
    if(role == "penyihir"){
        console.log(welcome + nama);
        console.log("Halo " + role + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
    } else if(role == "guard"){
        console.log(welcome + nama);
        console.log("Halo " + role + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    } else if(role == "werewolf"){
        console.log(welcome + nama);
        console.log("Halo " + role + " " + nama + ", Kamu akan memakan mangsa setiap malam!");
    } else {
        console.log("Halo " + nama + " Role tidak ditemukan");
    }
}


console.log("Soal Switch Case");

var tanggal = 21; 
var bulan = 12; 
var tahun = 1945;

switch (bulan) {
    case 1:
        console.log(tanggal + " januari " + tahun);
        break;
    case 2:
        console.log(tanggal + " februari " + tahun);
        break;
    case 3:
        console.log(tanggal + " maret " + tahun);
        break;
    case 4:
        console.log(tanggal + " april " + tahun);
        break;
    case 5:
        console.log(tanggal + " mei " + tahun);
        break;
    case 6:
        console.log(tanggal + " juni " + tahun);
        break;
    case 7:
        console.log(tanggal + " juli " + tahun);
        break;
    case 8:
        console.log(tanggal + " agustus " + tahun);
        break;
    case 9:
        console.log(tanggal + " september " + tahun);
        break;
    case 10:
        console.log(tanggal + " oktober " + tahun);
        break;
    case 11:
        console.log(tanggal + " november " + tahun);
        break;
    case 12:
        console.log(tanggal + " desember " + tahun);
        break;
        
    default:
        console.log("Bulan tidak valid");
        break;
}




