var express = require('express');
var router = express.Router();

/* GET home page. */
const VenuesController = require('../controller/venuesController')

//add venues
router.post("/", VenuesController.store);
router.get("/", VenuesController.index);
router.get("/:id", VenuesController.showid);
router.put("/:id", VenuesController.update);
router.delete("/:id", VenuesController.destroy);





// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

module.exports = router;
