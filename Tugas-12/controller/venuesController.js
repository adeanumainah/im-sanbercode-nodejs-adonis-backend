//import model
const e = require('express')
const { where } = require('sequelize')
const {Venues}= require('../models')

class VenuesController{
    //fungsi tambah data
    static async store(req, res){
        try {

        let name = req.body.name
        let address = req.body.address
        let phone = req.body.phone

        const newVenues = await Venues.create({
            name : name,
            address : address,
            phone : phone
        })

        res.status(200).json({
            status : "success",
            message : "Berhasil ditambahkan"
        })
        } catch (error) {
            res.status(401).json({
                status : "failed",
                message : "gagal menyimpan data",
                msg: error.errors.map((e) => e.message)
            })
            
        }

    }

    static async index(req, res){
        const venues = await Venues.findAll();

        res.status(200).json({
            message : "Success",
            data : venues
        })
    }

    static async showid(req, res){
        try {
            let idVenues = req.params.id;
            
            let dataVenuesDetail = await Venues.findByPk(idVenues)

            res.status(200).json({
                status : "success",
                data : dataVenuesDetail
            })
        
        } catch (error) {
            res.status(401).json({
                status : "failed",
                message : "data gagal ditampilkan, id tidak ditemukan",
                msg : error
            })
        }
    }

    static async update(req, res){
        try {

        let name = req.body.name
        let address = req.body.address
        let phone = req.body.phone

        await Venues.update({
            name : name,
            address : address,
            phone : phone,
        }, 
        {
        where : {
            id: req.params.id
        }
    }
        )

        res.status(200).json({
            status : "success",
            message : "berhasil update data",
            msg: error.errors.map((e) => e.message)
        })

        
        } catch (error) {
            res.status(401).json({
                status : "failed",
                message : "gagal menyimpan data",
                msg: error.errors.map((e) => e.message)
            })
            
        }

    }

    static async destroy(req, res){
        try {
            await Venues.destroy({
                where : {
                    id: req.params.id
                }
            });
            res.status(200).json({
                status : "success",
                message : "berhasil menghapus data",
                msg: error
            })
        } catch (error) {
            res.status(401).json({
                status : "failed",
                message : "gagal menghapus data",
                msg: error
            })
        }
    }
}

module.exports = VenuesController;