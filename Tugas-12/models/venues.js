'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Venues extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Venues.init({
    name: {
      type : DataTypes.STRING,
      allowNull : false,
      validate:{
        notNull : {
          msg : "input tidak boleh kosong"
        }
      }},
    address: {
      type : DataTypes.STRING,
      allowNull : false,
      validate : {
        notNull : {
          msg : "input address tidak boleh kosong"
        }
      }},
    phone: {
      type : DataTypes.STRING,
      allowNull : false,
      validate : {
        notNull : {
          msg : "input phone tidak boleh kosong"
        }
      }
    }
  }, {
    sequelize,
    modelName: 'Venues',
  });
  return Venues;
};