import Employee from "./employee";
import fs from "fs";
import "core-js/stable";
import fsPromises from "fs/promises";

const path = "data.json";

class Bootcamp{
    static register(input){
        let [ name, password, role ] = input.split(',');
        fs.readFile(path,(err, data)=>{
            if(err){
                console.log(err)
            }

            let existingData = JSON.parse(data);
            let employee = new Employee(name, password, role);
            existingData.push(employee);
            fs.writeFile(path, JSON.stringify(existingData), (err)=> {
                if (err) {
                    console.log(err);
                } else {
                    console.log("berhasil register");
                }
            });
        });
    }

    static login(input){
        let[ name, password ] = input.split(",");

        fsPromises.readFile(path).then((data) => {
            let employees = JSON.parse(data);
            
            let indexEmp = employees.findIndex((emp)=> emp._name == name);

            if(indexEmp == -1){
                console.log("Data tidak ditemukan");
            } else {
                let employee = employees[indexEmp]

                if(employee._password == password){
                    employee._isLogin = true;

                    employees.splice(indexEmp, 1, employee);
                    return fsPromises.writeFile(path, JSON.stringify(employees));
                } else {
                    console.log("Password yang dimasukan salah");
                }
            }
        })
        // .then(()=>{
        //     console.log("Berhasil Login");
        // })
        .catch((err) => {
            console.log(err)
        });
    }
}

export default Bootcamp;