import Kelas from "./libs/kelas";
import Student from "./libs/student";

const kelas = new Kelas("public");

kelas.createClass("Laravel", "Beginner", "Abduh")
kelas.createClass("React", "Beginner", "Abdul")

console.log(kelas.bootcamp)

let names = ["regi", "ahmad", "bondra", "iqbal", "putri", "rezky"]

names.map((student, index) => {
    let newStud = new Student(student)
    let bootcamp = kelas.bootcamp[index % 2].name
    kelas.insertStudent(bootcamp, newStud)
  })

  // menampilkan data kelas dan student nya
  kelas.bootcamp.forEach(post => {
    console.log(post)
  });


