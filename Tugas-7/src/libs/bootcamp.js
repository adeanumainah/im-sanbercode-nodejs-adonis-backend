class Bootcamp{
    constructor(name, level, instructor){
        this._name = name
        this._students = []
        this._level = level
        this._instructor = instructor 
        
    }

    get name(){
        return this._name
    }

    set name(strname){
        this._name = strname
    }

    get level(){
        return this._level
    }

    set level(strlevel){
        this._level = strlevel
    }

    get instructor(){
        return this._instructor
    }

    set instructor(strinstructor){
        this._instructor = strinstructor
    }

    get students(){
        return this._students
    }

    addStudents(objStudents){
        this._students.push(objStudents)
    }
}

export default Bootcamp;