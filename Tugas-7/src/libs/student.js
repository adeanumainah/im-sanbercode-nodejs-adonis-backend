class Student{
    constructor(students){
        this._student = students
    }

    get student(){
        return this._student
    }

    set student(student){
        this._student = student
    }
}

export default Student;