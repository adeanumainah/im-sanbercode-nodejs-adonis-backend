import Bootcamp from "./bootcamp"

class Kelas{
    constructor(kelas){
        this._kelas = kelas
        this._bootcamp = []
    }

    get kelas(){
        return this._kelas
    }

    set kelas(kelas){
        this._kelas = kelas
    }

    get bootcamp(){
        return this._bootcamp 
    }

    createClass(name, level, instructor){
        let newBootcamp = new Bootcamp(name, level, instructor)
        this._bootcamp.push(newBootcamp)
    }

    insertStudent(namaBootcamp, obgStudent){
        let caribootcamp = this._bootcamp.find((post) => post.name === namaBootcamp)
        caribootcamp.addStudents(obgStudent)
    }
}

export default Kelas;